//: Playground - noun: a place where people can play

import UIKit


let boardSize = 500
var totalSnakes: Int = 10
var totalLadders: Int = 5
var board:[String] = []
var step:Int = 0

var dictBoard: [String : String] = [:]


class Player {
    var name: String = "Player"
    var position: Int = 1
    var newPosition: Int = 1
    var winner: Bool = false
    var hitSnake: [String] = []
    var hitLadder: [String] = []
    init(name: String){
        self.name = name
    }
}

var players: [Player] = [Player(name: "Kenny"),
                        Player(name: "Cathy "),
                        Player(name: "Grace "),
                        Player(name: "Sky   ")]


func rollDice() -> Int{
    return Int(arc4random_uniform(6)) + 1
}

func getDictBoard(){
    var usedPosition: [String] = []
    var positionToFill: Int = 0
    var position2ToFill: Int = 0
    
    usedPosition.append("\(boardSize)")
    usedPosition.append("\(0)")

    // build up snakes and ladders
    while ( totalSnakes != 0 || totalLadders != 0 ){
        var quit1: Bool = true
        repeat{
            positionToFill = Int( arc4random_uniform(UInt32(boardSize))) + 1
            if !usedPosition.contains("\(positionToFill)"){
                usedPosition.append("\(positionToFill)")
                quit1 = true
                                break
        } else {
                quit1 = false
            }
        }while(!quit1)
        
        var quit2: Bool = true
        repeat{
            position2ToFill = Int( arc4random_uniform(UInt32(boardSize))) + 1
            if !usedPosition.contains("\(position2ToFill)"){
                usedPosition.append("\(position2ToFill)")
                quit2 = true
                break
        } else {
                quit2 = false
            }
        }while(!quit2)
        
        if positionToFill > position2ToFill {
            if totalLadders != 0 {
                dictBoard["\(positionToFill)"] = "SnakeHead\(totalLadders)"
                dictBoard["\(position2ToFill)"] = "SnakeTail\(totalLadders)"
                totalLadders--
            }
            
        } else {
            if totalSnakes != 0 {
                dictBoard["\(positionToFill)"]  = "LadderHead\(totalSnakes)"
                dictBoard["\(position2ToFill)"]  = "LadderTail\(totalSnakes)"
                totalSnakes--
            }
        }
    }
}

func getNewPositionName( position: String) -> String{
    if let val = dictBoard[position]  {
        return val
    } else {
        return "-1"
    }
}

func getNewPositionNumber ( name: String) -> Int {
    for  key in dictBoard.keys{
        if dictBoard[key] == name{
            return Int(key)!
        }
    }
    return 0
}

getDictBoard()

var roundCount: Int = 1
var isEnd: Bool = false

while(!isEnd) {
    print("---------------------------- Round \(roundCount)---------------------------- ")
    roundCount++
    
    for player in players{
        var oldPosition: Int = player.position
        var diceNumber: Int = rollDice();
        var newPosition: Int = oldPosition + diceNumber
        var event: String = ""
        
        if let newName: String = getNewPositionName("\(newPosition)") {
            if (newName != "-1") && (newName.containsString("Head")) {
               var tail =  newName.self.stringByReplacingOccurrencesOfString("Head", withString:"Tail")
                
                var nNewPosition = getNewPositionNumber(tail)
                
                if newPosition < nNewPosition{
                    player.hitLadder.append(newName)
                    event = " climb Ladder at " + newName
                } else {
                    player.hitSnake.append(newName)
                    event = " hit Snake at " + newName
                }
                
                newPosition = nNewPosition
                newName.self.stringByReplacingOccurrencesOfString("Head", withString:"Tail")
            }
        }
        
        player.position = newPosition
        
        print("\t\(player.name) rolled \t \(diceNumber) and move to \t \(player.position) " + event)
        
        if player.position >= boardSize{
            player.winner = true
            isEnd = true
            print("\(player.name) wins!!!\n")
            break
        }
    }
    
}

// show result summary
for player in players{
    print("\(player.name) \(player.winner ? "Win" : "lose") ladder: \(player.hitLadder.count) snake: \(player.hitSnake.count)." )
}

dictBoard


func printBoard(){
    for ( var i = 0 ; i < boardSize ; i++ ){
        
        if i % 20 == 0{
            print( "")
        } else {
            print ( i )
        }
    }
}


